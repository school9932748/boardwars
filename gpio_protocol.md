# GPIO_Protocol

This document describes the protocol between the webbrowser and the pico 8 game.
This is done via the GPIO api in pico-8.

This normally used for running pico-8 on microcontrollers, but can also be used ro read and write memory from both pico-8, and the webbrowser

## Bytes:

| Index | Meaning      |
| ----- | ------------ |
| 0     | Empty byte   |
| 1     | address byte |
| 2     | status byte  |
| 3     | tag          |
| 4-7   | values       |

## Status:

| Byte | Meaning                |
| ---- | ---------------------- |
| 0x00 | Clear / ready for user |
| 0x01 | GPIO is being written  |
| 0x02 | GPIO is filled         |
| 0x03 | GPIO is being read     |

## Address:

| Byte | Address    |
| ---- | ---------- |
| 0x00 | empty      |
| 0x01 | pico-8     |
| 0x02 | webbrowser |

## Tags:

| Tag  | Operation | Meaning         | byte 4          | byte 5      | byte 6 | byte 7 |
| ---- | --------- | --------------- | --------------- | ----------- | ------ | ------ |
| 0x01 | Read      | Draw card       | Card suit       | Card number |        |        |
| 0x02 | Write     | Play card       | Card index      |             |        |        |
| 0x03 | Read      | Opponent_amount | Amount of cards |             |        |        |
| 0x04 | Read      | set top card    | Card suit       | Card number |        |        |
| 0x05 | Read      | My cards amount | Amount of cards |             |        |        |
| 0x20 | Write     | Sync request    | Tag to get      |             |        |        |
| 0x21 | Read      | Sync done       |                 |             |        |        |
