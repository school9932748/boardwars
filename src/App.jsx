import { CssVarsProvider, Typography } from "@mui/joy";
import {
  Router,
  Routes,
  Route,
  BrowserRouter,
  useNavigate,
} from "react-router-dom";
import NavDrawer from "./components/NavDrawer";
import { useEffect, useState } from "react";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import LoginForm from "./components/LoginForm";
import SignUpForm from "./components/SignUpForm";
import PageHome from "./layouts/PageHome";
import Default from "./layouts/Default";
import Theme from "./Theme";
import JoinRandomMatch from "./components/JoinRandomMatch";
import Match from "./components/Match";
import PagePlay from "./layouts/PagePlay";
import Redirect from "./components/Redirect";
import PageAccount from "./layouts/PageUsers";
import PageUsers from "./layouts/PageUsers";
import Messages from "./layouts/Messages";
import History from "./layouts/History";

function App(props) {
  const [isLoggedIn, setLoggedIn] = useState(false);

  document.title = "Boardwars";

  onAuthStateChanged(getAuth(), (user) => {
    if (user) {
      setLoggedIn(true);
    } else {
      setLoggedIn(false);
    }
  });

  return (
    <>
      <CssVarsProvider theme={Theme}>
        <BrowserRouter>
          <NavDrawer isLoggedIn={isLoggedIn} />
          <Routes>
            <Route path="/" element={<Redirect location="/home" />} />
            <Route
              path="/home"
              element={
                <Default>
                  <PageHome />
                </Default>
              }
            />

            <Route
              path="/login"
              element={
                <Default>
                  <LoginForm />
                </Default>
              }
            />
            <Route
              path="/sign-up"
              element={
                <Default>
                  <SignUpForm/>
                </Default>
              }
            />
            <Route
              path="/play"
              element={
                <Default>
                  <PagePlay />
                </Default>
              }
            />
            <Route
              path="/users"
              element={
                <Default>
                  <PageUsers/>
                </Default>
              }
            />
            <Route
              path="/match/join"
              element={
                <Default>
                  <JoinRandomMatch />
                </Default>
              }
            />
            <Route
              path="/matches/:id"
              element={
                <Default>
                  <Match />
                </Default>
              }
            />
            <Route
              path="/messages"
              element={
                <Default>
                  <Messages />
                </Default>
              }
            />
            <Route
              path="/history"
              element={
                <Default>
                  <History />
                </Default>
              }
            />
          </Routes>
        </BrowserRouter>
      </CssVarsProvider>
    </>
  );
}

export default App;
