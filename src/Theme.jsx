import { extendTheme } from "@mui/joy/styles";

// realtime colors: https://www.realtimecolors.com/?colors=100b09-faf8f5-ee6d17-e4b596-e2915a&fonts=Inter-Inter

const primary = {
  50: "#fdf0e7",
  100: "#fbe1d0",
  200: "#f8c3a0",
  300: "#f4a571",
  400: "#f18741",
  500: "#ed6a12",
  600: "#be540e",
  700: "#8e3f0b",
  800: "#5f2a07",
  900: "#2f1504",
};

const secondary = {
  50: "#faf1eb",
  100: "#f5e2d6",
  200: "#eac6ae",
  300: "#e0a985",
  400: "#d58d5d",
  500: "#cb7034",
  600: "#a25a2a",
  700: "#7a431f",
  800: "#512d15",
  900: "#29160a",
};

const accent = {
  50: "#fbf0e9",
  100: "#f7e2d4",
  200: "#f0c5a8",
  300: "#e8a87d",
  400: "#e08b52",
  500: "#d96e26",
  600: "#ad581f",
  700: "#824217",
  800: "#572c0f",
  900: "#2b1608",
};

const background = {
  50: "#f6f3ee",
  100: "#eee7dd",
  200: "#ddcfbb",
  300: "#ccb899",
  400: "#bba077",
  500: "#aa8855",
  600: "#886d44",
  700: "#665233",
  800: "#443622",
  900: "#221b11",
};

const Theme = extendTheme({
  colorSchemes: {
    light: {
      palette: {
        primary: primary,
        secondary: secondary,
        accent: accent,
        background: background
      },
    },
    dark: {
      palette: {},
    },
  },
});

export default Theme;