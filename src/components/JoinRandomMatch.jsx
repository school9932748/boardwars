import { CircularProgress, Typography } from "@mui/joy";
import { addDoc, arrayUnion, collection, doc, getDoc, getDocs, query, updateDoc, where } from "firebase/firestore";
import { useState, useEffect } from "react";
import firebase from "../firebase";
import { useNavigate } from "react-router-dom";
import { getAuth } from "firebase/auth";
import axios from "axios";

const JoinRandomMatch = () => {

    const [ detailText, setDetailText ] = useState("Loading matches");
    const [ matchFound, setMatchFound ] = useState(false);
    const navigate = useNavigate();

    const join_match = async () => {

        const userRef = doc(firebase.db, "users", firebase.auth.currentUser.uid);
        const userDoc = await getDoc(userRef);

        // find all matches with room
        const q = query(
          collection(firebase.db, "matches"),
          where("isFull", "==", false)
        );
        const snapshot = await getDocs(q);
        const docsArray = snapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        }));

        // if no matches are found with room, create a new one
        if (docsArray.length == 0) {
            let word = "";
            await axios.get("https://random-word-api.herokuapp.com/word?length=5")
            .then(response => {
                word = response.data[0]
            })
            .catch(error => {

            });

            setDetailText("No matches found, creating new");
            const newMatch = {
                isFull: false,
                users: [{id: firebase.auth.currentUser.uid, displayName: userDoc.data().displayName, score: 0, text: ""}],
                index: 0,
                totalPlayers: 1,
                word: word,
                is_done: false
            }
            const docRef = await addDoc(collection(firebase.db, 'matches'), newMatch).then((docRef) => {
                navigate("/matches/" + docRef.id);
            }).catch((error) => {
                setDetailText("Something went wrong, try again later :(")
            });
        // else join the first found match
        } else {
            setDetailText("Match found, joining");

            const docRef = doc(firebase.db, "matches", docsArray[0].id);
            await updateDoc(docRef, {
                users: arrayUnion({id: getAuth().currentUser.uid, displayName: userDoc.data().displayName, score: 0, text: ""}),
                isFull: true
            });

            navigate("/matches/" + docsArray[0].id);

        }
    
      };
    
      useEffect(() => {
        join_match();
      }, [window.location.href]);

    return (
        <>
            <Typography level="h1">{detailText}</Typography>
            <CircularProgress size="lg"/>
        </>
    )
};

export default JoinRandomMatch;