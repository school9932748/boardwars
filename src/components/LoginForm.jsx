import { Stack, Input, Button, Typography, Card, Divider, Snackbar, Grid } from "@mui/joy";
import user from "../controller/User";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [wrongCred, setWrongCred] = useState(false);
  const navigate = useNavigate();

  const submit = async () => {
    setLoading(true);
    setWrongCred(false);
    const uid = await user.login(mail, password);
    console.log(uid);
    if (uid) {
      navigate("/home");
    } else {
      setWrongCred(true);
    }
    setLoading(false);
  };

  return (
    <Card
    sx={{
      width: "90%",
      maxWidth: "300px",
    }}
    >
      <Snackbar
      open={wrongCred}
      anchorOrigin={{vertical: 'top', horizontal: 'center'}}
      onClose={() => setWrongCred(false)}
      >
        <Typography color="danger">
          Wrong credentials
        </Typography>
      </Snackbar>
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={2}
      >
        <Typography level="h3">Login</Typography>
        <Input sx={{
          width: "90%"
        }}
          onChange={(event) => setMail(event.target.value)}
          placeholder="email"
        />
        <Input sx={{
          width: "90%"
        }}
          onChange={(event) => setPassword(event.target.value)}
          placeholder="password"
          type="password"
        />
        <Divider />
        <Button loading={loading} onClick={submit} sx={{
          width: "90%"
        }}>
          Login
        </Button>
        <Button 
        variant="soft"
        sx={{
          width: "90%"
        }} onClick={() => navigate("/sign-up")}>
          Create account
        </Button>

      </Stack>
    </Card>
  );
};

export default LoginForm;
