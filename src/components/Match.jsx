import {
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardCover,
  CircularProgress,
  Divider,
  IconButton,
  List,
  ListItem,
  Sheet,
  Stack,
  Typography,
  Input,
  LinearProgress,
} from "@mui/joy";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import firebase from "../firebase";
import {
  collection,
  doc,
  onSnapshot,
  getDoc,
  updateDoc,
  arrayUnion,
  FieldValue,
  increment,
} from "firebase/firestore";
import User from "./matchComponents/User";
import { Canvas } from "@react-three/fiber";
import Engine from "./matchComponents/Engine";
import { CallToAction, Casino, Margin, ViewCarousel } from "@mui/icons-material";
import Pico8 from "react-pico-8";
import "react-pico-8/styles.css";
import { getAuth } from "firebase/auth";
import axios from "axios";
import wikitext2markdown from "wikitext2markdown";
import markdownit from "markdown-it";

const Match = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);

  const [winner, setWinner] = useState("");
  
  const [submitting, setSubmitting] = useState(false);

  const [match, setMatch] = useState(null);


  const isMe = (id) => {
    if (id === getAuth().currentUser.uid) {
      return true;
    } else {
      return false;
    }
  };

  const getIndex = () => {
    for (let i = 0; i < match.users.length; i++) {
      if (match.users[i].id === firebase.auth.currentUser.uid) {
        return i;
      }
    }
    return 0;
  };

  const setText = (e) => {
    if (e.target.value.length < 6) {
      const docRef = doc(firebase.db, "matches", params.id);
      let newMatch = match;
      newMatch.users[getIndex()].text = e.target.value;
      updateDoc(docRef, newMatch);
    }
  };

  const publishText = async (e) => {
    if (e.code == "Enter" && e.target.value.length === 5) {
      setSubmitting(true);
      let newMatch = match;
      const docRef = doc(firebase.db, "matches", params.id);
      
      if (e.target.value === match.word) {
        if (match.history) {
          newMatch.history = match.history;
          newMatch.history.push({winner: getIndex(), word: match.word});
        } else {
          newMatch.history = [{winner: getIndex(), word: match.word}];
        }

        newMatch.users[getIndex()].score = match.users[getIndex()].score + 20;


        // win condition
        if (newMatch.users[getIndex()].score >= 100) {
          newMatch.is_done = true;
          const docRef = doc(firebase.db, "users", firebase.auth.currentUser.uid);
          await updateDoc(docRef, {wins: increment(1)});
        } else {
          newMatch.user_0_guesses = [];
          newMatch.user_1_guesses = [];
  

          await axios.get("https://random-word-api.herokuapp.com/word?length=5")
          .then(response => {
              newMatch.word = response.data[0]
          })
          .catch(error => {
  
          });
        }
      } else {
        if (getIndex() == 0) {
          if (match.user_0_guesses) {
            newMatch.user_0_guesses = match.user_0_guesses;
            newMatch.user_0_guesses.push(e.target.value);
          } else {
            newMatch.user_0_guesses = [e.target.value];
          }
        } else {
          if (match.user_1_guesses) {
            newMatch.user_1_guesses = match.user_1_guesses;
            newMatch.user_1_guesses.push(e.target.value);
          } else {
            newMatch.user_1_guesses = [e.target.value];
          }
        }
      }


      newMatch.users[getIndex()].text = "";
      await updateDoc(docRef, newMatch);
      setSubmitting(false);
    }
  }

  const letterStyle = {
    width: "30px",
    height: "30px",
    padding: "10px",
    borderRadius: "5px"
  }

  useEffect(() => {
    if (!match) {
      return;
    }
    if (match.is_done) {
      if (match.users[1]) {
        if (match.users[0].score > match.users[1].score) {
          setWinner(match.users[0].displayName);
        } else {
          setWinner(match.users[1].displayName);
        }
      } else {
        setWinner(match.users[0].displayName);
      }
    }
  },[match]);

  const render_guesses = (i) => {
    let html = [];
    let guesses = [];
    let cont = false;
    if (i === 0) {
      if (match.user_0_guesses) {
        guesses = match.user_0_guesses;
        cont = true;
      }
    } else {
      if (match.user_1_guesses) {
        guesses = match.user_1_guesses;
        cont = true;
      }
    }

    if (!cont) {
      return;
    }

    guesses.forEach((guess, index) => {
      if (index > guesses.length - 6) {
        let subhtml = [];
        for (let i = 0; i < 5; i++) {
          if (guess[i] == match.word[i]) {
            subhtml.push(
              <Typography
                level="h3"
                variant="solid"
                color="success"
                sx={letterStyle}
                key={index * 100 + i}>
                {guess[i]}
              </Typography>)
          } else if (match.word.includes(guess[i])) {
            subhtml.push(
              <Typography
                level="h3"
                variant="solid"
                color="warning"
                sx={letterStyle}
                key={index * 100 + i}>
                {guess[i]}
              </Typography>)
          } else {
            subhtml.push(
              <Typography
                level="h3"
                variant="solid"
                sx={letterStyle}
                key={index * 100 + i}>
                {guess[i]}
              </Typography>)
          }
        }
        html.push(<Stack
          sx={{
            width: "100%"
          }}
          direction="row"
          justifyContent="space-evenly"
          alignItems="center"
          spacing={2}
        >{subhtml.map((element) => { return element })}</Stack>)
      }
    });

    return html;
  }

  useEffect(() => {
    const docRef = doc(firebase.db, "matches", params.id);

    const unSub = onSnapshot(
      docRef,
      (doc) => {
        if (doc.exists()) {
          setMatch(doc.data());
        } else {
        }
        setLoading(false);
      },
      (error) => {
        console.error(error);
        setLoading(false);
      }
    );

    return () => unSub();
  }, [params.id]);

  if (loading) {
    return <></>;
  }

  if (match) {
    return (
      <>
        <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          sx={{
            width: "100%",
          }}
          spacing={2}
        >
          {match.is_done && (
            <Stack>
              <Typography>
                Winner: {winner}
              </Typography>
              <Button onClick={() => navigate("/home")}>
                Home
              </Button>
            </Stack>
          )}
          {match.history && match.history.map((element) => {
            return (
              <Card>
                <Typography>
                  {element.word}
                </Typography>
              </Card>
            )
          })}
        </Stack>
        <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          sx={{
            width: "100%",
          }}
          spacing={2}
        >
          {match.users.map((user, index) => {
            return (
              <Card
                key={index}
                variant={isMe(user.id) ? "outlined" : "plain"}
              >
                <Stack
                  direction="column"
                  justifyContent="center"
                  alignItems="center"
                  spacing={2}
                  sx={{
                    width: "100%"
                  }}
                >
                  {render_guesses(index)}
                </Stack>
                <Typography level="h3">{user.displayName}</Typography>
                <CircularProgress
                  variant="outlined"
                  size="lg"
                  determinate
                  value={user.score}
                >
                  {user.score}
                </CircularProgress>
                <Input
                  onChange={setText}
                  onKeyDown={publishText}
                  value={user.text}
                  loading={(isMe() && submitting).toString()}
                  disabled={(user.id !== getAuth().currentUser.uid || match.is_done)}
                ></Input>
              </Card>
            );
          })}
        </Stack>
      </>
    );
  } else {
    return (
      <>
        <Typography level="h1" color="danger">
          Match not found!
        </Typography>
        <Button
          color="danger"
          onClick={() => {
            navigate("/home");
          }}
        >
          Return
        </Button>
      </>
    );
  }
};

export default Match;
