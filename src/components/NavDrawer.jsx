import {
  Drawer,
  IconButton,
  Typography,
  Stack,
  ListItemButton,
  List,
  ListItem,
  ListItemDecorator,
  Divider,
} from "@mui/joy";
import Menu from "@mui/icons-material/Menu";
import Close from "@mui/icons-material/Close";
import Home from "@mui/icons-material/Home";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { AccountCircle, Add, ChevronRight, History, Login, Message, VideogameAsset } from "@mui/icons-material";
import UserCard from "./UserCard";

const NavDrawer = (props) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const navigate = useNavigate();

  const link = (location) => {
    setDrawerOpen(false);
    if (window.location.pathname != location) {
      navigate(location);
    }
  };

  const linkFormat = (str) => {
    // Replace all '-' characters with '  '
    let formattedStr = str.replace(/-/g, '  ');
  
    // Capitalize the first character and every character following '  '
    formattedStr = formattedStr.replace(/\b\w/g, char => char.toUpperCase());
  
    return formattedStr;
  };

  const get_page_link = (location, startDecorator) => {
      return (
        <ListItem>
          {startDecorator && (
            <ListItemDecorator>{startDecorator}</ListItemDecorator>
          )}
          <ListItemButton onClick={() => {link("/" + location)}}>{linkFormat(location)}</ListItemButton>
          <ListItemDecorator><ChevronRight/></ListItemDecorator>
        </ListItem>
      );
  };

  return (
    <>
      <IconButton size="lg" onClick={() => setDrawerOpen(true)}>
        <Menu />
      </IconButton>
      <Drawer
        open={drawerOpen}
        onClose={() => {
          setDrawerOpen(false);
        }}
      >
        <Stack
          direction="column"
          justifyContent="flex-start"
          alignItems="center"
          spacing={2}
        >
          <Stack
            direction="row"
            justifyContent="flex-end"
            alignItems="center"
            spacing={2}
            sx={{
              width: "100%",
            }}
          >
            <IconButton
              size="lg"
              onClick={() => {
                setDrawerOpen(false);
              }}
            >
              <Close />
            </IconButton>
          </Stack>
          <Typography level="h1">BoardWars</Typography>
          <Divider/>
          <UserCard isLoggedIn={props.isLoggedIn}></UserCard>
          <List
            size="lg"
            sx={{
              width: "80%"
            }}
          >
            {get_page_link("home", <Home />)}
            {get_page_link("history", <History />)}
            {props.isLoggedIn && get_page_link("Play",<VideogameAsset/>)}
            {props.isLoggedIn && get_page_link("Users",<AccountCircle/>)}
            {props.isLoggedIn && (<>{get_page_link("messages",<Message/>)}</>)}
            {!props.isLoggedIn && (<>{get_page_link("login",<Login/>)}</>)}
          </List>
        </Stack>
      </Drawer>
    </>
  );
};

export default NavDrawer;
