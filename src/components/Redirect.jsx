import { Typography } from "@mui/joy";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Redirect = (props) => {
    const navigate = useNavigate();

    useEffect(() => {
        navigate(props.location);
    });

    return (
        <Typography level="h1">Redirecting to {props.location}</Typography>
    )
}

export default Redirect;