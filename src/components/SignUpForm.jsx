import {
  Stack,
  Input,
  Button,
  Typography,
  Card,
  Divider,
  Snackbar,
  Grid,
  IconButton,
} from "@mui/joy";
import user from "../controller/User";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { WestRounded } from "@mui/icons-material";

const LoginForm = () => {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const [userName, setUserName] = useState("");
  const [loading, setLoading] = useState(false);
  const [exists, setExists] = useState(false);
  const navigate = useNavigate();

  const submit = async () => {
    setLoading(true);
    setExists(false);
    const success = await user.signUp(userName, mail, password, "user");
    if (success) {
      navigate("/home");
    } else {
      setExists(true);
    }
    setLoading(false);
  };

  return (
    <Card
      sx={{
        width: "90%",
        maxWidth: "300px",
      }}
    >
      <Snackbar
        open={exists}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        onClose={() => setExists(false)}
      >
        <Typography color="danger">
          username or mail is already in use
        </Typography>
      </Snackbar>
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={2}
      >
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          spacing={2}
          sx={{
            width: "90%"
          }}
        >
          <Typography level="h3">Create account!</Typography>
          <IconButton onClick={() => navigate("/login")}>
            <WestRounded />
          </IconButton>
        </Stack>

        <Input
          sx={{
            width: "90%",
          }}
          onChange={(event) => setUserName(event.target.value)}
          placeholder="username"
        />
        <Input
          sx={{
            width: "90%",
          }}
          onChange={(event) => setMail(event.target.value)}
          placeholder="email"
        />
        <Input
          sx={{
            width: "90%",
          }}
          onChange={(event) => setPassword(event.target.value)}
          placeholder="password"
          type="password"
        />
        <Divider />
        <Button
          loading={loading}
          onClick={submit}
          sx={{
            width: "90%",
          }}
        >
          Create Account
        </Button>
      </Stack>
    </Card>
  );
};

export default LoginForm;
