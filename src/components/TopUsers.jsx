import {
  Button,
  Card,
  Sheet,
  Typography,
  Stack,
  CircularProgress,
} from "@mui/joy";
import Trophy from "@mui/icons-material/EmojiEvents"
import { useEffect, useState } from "react";
import { collection, getDocs, limit, orderBy, query } from "firebase/firestore";
import firebase from "../firebase";

const TopTen = (props) => {
  const [top, setTop] = useState([]);
  const [loading, setLoading] = useState(false);

  const get_top_ten = async () => {
    setLoading(true);
    setTop([]);
    const q = query(
      collection(firebase.db, "users"),
      orderBy("wins", "desc"),
      limit(10)
    );
    const snapshot = await getDocs(q);
    const docsArray = snapshot.docs.map((doc) => ({
      id: doc.id,
      data: doc.data(),
    }));

    setTop(docsArray);
    setLoading(false);
  };

  useEffect(() => {
    get_top_ten();
  }, [window.location.href]);

  return (
    <Card sx={props.sx}>
      <Typography startDecorator={<Trophy/>} variant="solid" color="primary" level="h3">
        Top Players
      </Typography>
      {loading && (
        <Stack
          justifyContent="center"
          alignItems="center"
          sx={{
            width: "100%",
            height: "100%",
          }}
        >
          <CircularProgress size="lg"/>
        </Stack>
      )}
      {top.map(function (user) {
        return (
          <Typography key={user.id}>
            {user.data.displayName} - {user.data.wins}
          </Typography>
        );
      })}
    </Card>
  );
};

export default TopTen;
