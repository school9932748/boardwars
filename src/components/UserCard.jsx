import { ButtonGroup, Card, IconButton, Stack, Typography } from "@mui/joy";
import user from "../controller/User";
import { Logout, ManageAccounts, Paid, SportsScore } from "@mui/icons-material";
import { useEffect, useState } from "react";
import firebase from "../firebase";
import { Gauge } from "@mui/x-charts";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { doc, getDoc } from "firebase/firestore";

const UserCard = (props) => {
  const [wins, setWins] = useState(0);
  const [userName, setUserName] = useState("");

  useEffect(() => {
    const func = async () => {
      const docRef = doc(firebase.db, "users", firebase.auth.currentUser.uid);
      const snapShot = await getDoc(docRef);
      if (snapShot) {
        setWins(snapShot.data().wins);
        setUserName(snapShot.data().displayName);
      }
    }
    if (props.isLoggedIn) {
      func();
    }
  })



  if (props.isLoggedIn) {
    return (
      <Card
        variant="soft"
        sx={{
          width: "80%",
        }}
      >
        <Stack direction="row" justifyContent="space-between">
          <Stack
            direction="column"
            justifyContent="space-between"
            alignItems="flex-start"
            spacing={2}
          >
            <Typography>{userName}</Typography>
            <Typography startDecorator={<SportsScore />}>{wins}</Typography>
          </Stack>
          <Stack
            direction="row"
            justifyContent="flex-end"
            alignItems="center"
            spacing={2}
          >
            <ButtonGroup orientation="vertical">
              <IconButton
                onClick={() => {
                  user.logout();
                }}
                variant="solid"
                color="primary"
              >
                <Logout />
              </IconButton>
            </ButtonGroup>
          </Stack>
        </Stack>
      </Card>
    );
  } else {
    return <></>;
  }
};

export default UserCard;
