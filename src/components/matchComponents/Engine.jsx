import { CircularProgress, Input } from "@mui/joy";

function Engine() {
  return (
    <>
      <CircularProgress variant="outlined" size="lg" determinate value={58}>
        58
      </CircularProgress>
      <Input></Input>
    </>
  )
};

export default Engine;