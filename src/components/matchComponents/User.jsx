import { Card, Typography } from "@mui/joy";


const User = (props) => {


    return (
        <Card>
            <Typography>{props.displayName}</Typography>
        </Card>
    )
}

export default User;