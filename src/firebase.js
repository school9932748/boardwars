// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics"; 
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAjF33aMiUHUZ__bEja1vzc7WgrKpEL5PA",
    authDomain: "boardwars-8fb35.firebaseapp.com",
    databaseURL: "https://boardwars-8fb35-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "boardwars-8fb35",
    storageBucket: "boardwars-8fb35.appspot.com",
    messagingSenderId: "876886376199",
    appId: "1:876886376199:web:fccf49d193389a304b8fa2"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const firebase = {
    db: getFirestore(app),
    auth: getAuth(app)
}

export default firebase;