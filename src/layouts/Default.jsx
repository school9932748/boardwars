import { Sheet, Stack } from "@mui/joy";

const Default = (props) => {
  return (
    <Stack
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={2}
    >
      <Sheet
        sx={{
          width: "90%",
          minHeight: "90vh",
          borderRadius: "10px",
        }}
        variant="soft"
      >
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={2}
          sx={{
            minHeight: "80vh",
          }}
        >
          {props.children}
        </Stack>
      </Sheet>
    </Stack>
  );
};

export default Default;
