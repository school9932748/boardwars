import { Card, Stack, Typography } from "@mui/joy";
import { collection, doc, getDocs, query, where } from "firebase/firestore";
import { useEffect, useState } from "react";
import firebase from "../firebase";

const History = () => {

    const [matches, setMatches] = useState([]);

    useEffect(() => {
        const func = async () => {
            const q = query(
                collection(firebase.db, "matches"),
                where("is_done", "==", true)
            );
            const documents = await getDocs(q);
            const docsArray = documents.docs.map((doc) => {

                let winner;

                if (doc.data().users[1]) {
                    if (doc.data().users[0].score > doc.data().users[1].score) {
                        winner = 0;
                    } else {
                        winner = 1;
                    }
                } else {
                    winner = 0;
                }

                return {
                    id: doc.id,
                    data: doc.data(),
                    winner: winner
                }
            });
            setMatches(docsArray);
        }
        func();
    }, [])

    return (
        <>
            <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={2}
            >
                {
                    matches.map((element, index) => {
                        return (
                            <Card key={index}
                                sx={{
                                    width: "300px"
                                }}>
                                <Stack
                                    direction="row"
                                    justifyContent="space-between"
                                    alignItems="center"
                                    spacing={2}
                                >
                                    <Typography level="h2">Winner</Typography>
                                    <Typography level="h2">{element.data.users[element.winner].displayName}</Typography>
                                </Stack>
                                <Stack
                                    direction="row"
                                    justifyContent="space-between"
                                    alignItems="center"
                                    spacing={2}
                                >
                                    <Typography level="h3">Words</Typography>
                                    <Typography level="h3">Guessed by</Typography>
                                </Stack>
                                {
                                    element.data.history.map((el, index) => {
                                        return (
                                            <Stack
                                                direction="row"
                                                justifyContent="space-between"
                                                alignItems="center"
                                                spacing={2}
                                            >
                                                <Typography >
                                                    {el.word}
                                                </Typography>
                                                <Typography>
                                                    {element.data.users[el.winner].displayName}
                                                </Typography>
                                            </Stack>
                                        )
                                    })
                                }
                            </Card>
                        )
                    })
                }
            </Stack >
        </>
    )
}

export default History;