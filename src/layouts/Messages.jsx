import { useEffect, useState } from "react";
import firebase from "../firebase";
import { collection, deleteDoc, doc, getDoc, getDocs, onSnapshot, query, updateDoc, arrayUnion, where } from "firebase/firestore";
import { Button, ButtonGroup, Card, Stack, Typography } from "@mui/joy";

const Messages = () => {
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            if (firebase.auth.currentUser) {
                try {
                    const q = query(collection(firebase.db, "requests"), where("to", "==", firebase.auth.currentUser.uid));
                    const snapShot = await getDocs(q);
                    const docsArray = snapShot.docs.map((doc) => ({
                        id: doc.id,
                        data: doc.data()
                    }));
                    setMessages(docsArray);
                } catch (error) {
                    console.error("Error fetching documents: ", error);
                }
            }
        };

        fetchData();
    }, [firebase.auth.currentUser]);

    const accept = async (from) => {
        if (firebase.auth.currentUser) {
            const currentUserId = firebase.auth.currentUser.uid;
            
            // Reference to the current user's document
            const currentUserDocRef = doc(firebase.db, "users", currentUserId);
            
            // Reference to the 'from' user's document
            const fromUserDocRef = doc(firebase.db, "users", from);
            
            try {    
                // Update the current user's friends array
                updateDoc(currentUserDocRef, {
                    friends: arrayUnion(from)
                });
    
                // Update the 'from' user's friends array
                updateDoc(fromUserDocRef, {
                    friends: arrayUnion(currentUserId)
                });
        
                // Remove the request after accepting
                const requestDocRef = doc(firebase.db, "requests", from); // assuming 'from' is the request document ID
                await deleteDoc(requestDocRef);
    
                // Update local state to remove the accepted message
                setMessages((prevMessages) => prevMessages.filter(message => message.data.from !== from));
            } catch (error) {
                console.error("Error updating documents: ", error);
            }
        }
    };
    

    const decline = async (id) => {
        try {
            await deleteDoc(doc(firebase.db, "requests", id));
            setMessages((prevMessages) => prevMessages.filter(message => message.id !== id));
        } catch (error) {
            console.error("Error deleting document: ", error);
        }
    };

    return (
        <Stack>
            {messages.map((element) => {
                return (
                    <Card key={element.id}>
                        <Typography>{element.data.name} wants to be your friend!</Typography>
                        <ButtonGroup>
                            <Button onClick={() => accept(element.data.from)} color="success">Accept</Button>
                            <Button onClick={() => decline(element.id)} color="danger">Decline</Button>
                        </ButtonGroup>
                    </Card>
                );
            })}
            {!messages.length &&
                <Typography>Wow, no one want to be you friend!</Typography>
            }
        </Stack>
    );
};

export default Messages;
