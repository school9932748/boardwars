import { Button, Card, Sheet, Typography, Stack } from "@mui/joy";
import { useEffect, useState } from "react";
import firebase from "../firebase";
import TopTen from "../components/TopUsers";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useNavigate } from "react-router-dom";

const PageHome = () => {
  const [isLoggedIn, setLoggedIn] = useState(false);

  const navigate = useNavigate();

  onAuthStateChanged(getAuth(), (user) => {
    if (user) {
      setLoggedIn(true);
    } else {
      setLoggedIn(false);
    }
  });

  const list = {
    width: "200px",
    height: "300px",
  };

  return (
    <>
      <Stack
        direction="row"
        justifyContent="space-around"
        alignItems="center"
        spacing={2}
        sx={{
          width: "100%",
          height: "100%",
        }}
      >
        <TopTen sx={list} />

        <Card
          sx={{
            width: "200px",
            height: "100px",
          }}
        >
          <Stack
            direction="column"
            justifyContent="space-around"
            alignItems="center"
            spacing={2}
          >
            <Typography level="h3">Quick Play</Typography>
            <Button sx={{ width: "80%" }} onClick={() => navigate("/match/join")} disabled={!isLoggedIn}>
              New match
            </Button>
          </Stack>
        </Card>
      </Stack>
    </>
  );
};

export default PageHome;
