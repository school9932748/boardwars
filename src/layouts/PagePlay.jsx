import { useEffect, useState } from "react";
import firebase from "../firebase";
import { addDoc, collection, doc, getDoc, getDocs } from "firebase/firestore";
import { onAuthStateChanged } from "firebase/auth";
import { Button, ButtonGroup, Card, Stack, Typography } from "@mui/joy";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const PagePlay = () => {
  const [matches, setMatches] = useState([]);
  const [friends, setFriends] = useState([]);
  const [user, setUser] = useState({});
  const [isLoggedIn, setLoggedIn] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(firebase.auth, (user) => {
      if (user) {
        setLoggedIn(true);
      } else {
        setLoggedIn(false);
      }
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    const getMatches = async () => {
      const matchCollection = collection(firebase.db, "matches");
      const playerMatches = await getDocs(matchCollection);

      let docs = [];
      playerMatches.forEach((element) => {
        let add = false;
        element.data().users.forEach((user) => {
          if (user.id === firebase.auth.currentUser.uid) {
            add = true;
          }
        });

        if (element.data().is_done) {
          add = false;
        }

        if (add) {
          docs.push({ id: element.id, data: element.data() });
        }
      });
      setMatches(docs);
    };

    const getFriends = async () => {
      const docRef = doc(firebase.db, "users", firebase.auth.currentUser.uid);
      const snap = await getDoc(docRef);
      if (snap.exists() && snap.data().friends) {
        setUser(snap.data());
        const friendsIds = snap.data().friends;
        const friendsArray = [];

        for (const friend of friendsIds) {
          const friendDocRef = doc(firebase.db, "users", friend);
          const friendSnap = await getDoc(friendDocRef);
          if (friendSnap.exists()) {
            const newFriend = {
              name: friendSnap.data().displayName,
              id: friend
            };
            friendsArray.push(newFriend);
          }
        }

        setFriends(friendsArray);
      }
    };

    if (isLoggedIn) {
      getFriends();
      getMatches();
    }
  }, [isLoggedIn]);

  const playAgainstFriend = async (id, name) => {
    let word = "";
    await axios.get("https://random-word-api.herokuapp.com/word?length=5")
      .then(response => {
        word = response.data[0]
      })
      .catch(error => {

      });

    const newMatch = {
      isFull: false,
      users: [
        {
          id: firebase.auth.currentUser.uid,
          displayName: user.displayName,
          score: 0,
          text: ""
        },
        {
          id: id,
          displayName: name,
          score: 0,
          text: ""
        }
      ],
      index: 0,
      totalPlayers: 2,
      word: word,
      is_done: false,
      isFull: true
    }

    const docRef = await addDoc(collection(firebase.db, "matches"), newMatch);
    console.log(docRef.id);
    navigate("/matches/" + docRef.id)
  }

  return (
    <>
      <Stack
        direction="row"
        justifyContent="space-evenly"
        alignItems="center"
        spacing={2}
      >
        <Card
          sx={{
            width: "200px",
          }}
        >
          <Stack
            direction="column"
            justifyContent="space-around"
            alignItems="center"
            spacing={2}
          >
            <Typography level="h3">Quick Play</Typography>
            <Button
              sx={{ width: "90%" }}
              onClick={() => navigate("/match/join")}
              disabled={!isLoggedIn}
            >
              Join Random
            </Button>
          </Stack>
        </Card>
        <Card variant="outlined">
          <Typography level="h3">Friends</Typography>
          {friends.map((friend) => (
            <Card key={friend.id}>
              <Typography>{friend.name}</Typography>
              <Button onClick={() => { playAgainstFriend(friend.id, friend.name) }}>Play</Button>
            </Card>
          ))}
        </Card>
        <Card variant="outlined">
          <Typography level="h3">My Games</Typography>
          {!matches.length ? (
            <Typography>No Active Games</Typography>
          ) : (
            matches.map((match) => (
              <Card key={match.id} variant="outlined" color="primary">
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="center"
                  sx={{
                    width: "400px",
                  }}
                >
                  <Typography>{match.data.users[0].displayName} VS {match.data.users[1] ? match.data.users[1].displayName : "?"}</Typography>
                  <ButtonGroup orientation="vertical">
                    <Button
                      color="primary"
                      variant="solid"
                      onClick={() => navigate("/matches/" + match.id)}
                    >
                      Continue
                    </Button>
                    <Button variant="soft" color="danger">
                      Quit
                    </Button>
                  </ButtonGroup>
                </Stack>
              </Card>
            ))
          )}
        </Card>
      </Stack>
    </>
  );
};

export default PagePlay;
