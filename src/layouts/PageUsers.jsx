import { Button, Card, IconButton, Input, Stack, Tooltip, Typography } from "@mui/joy";
import { addDoc, collection, doc, getDoc, getDocs, query } from "firebase/firestore";
import { useEffect, useState } from "react";
import firebase from "../firebase";
import { HdrPlus, PersonAdd, PersonSearch } from "@mui/icons-material";

function PageUsers() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const func = async () => {
      const q = query(collection(firebase.db, "users"));
      const snapshot = await getDocs(q);
      const docsArray = snapshot.docs.map((doc) => ({
        id: doc.id,
        data: doc.data(),
      }));
      setUsers(docsArray);
    };

    func();
  }, []);

  const send_request = async (id) => {
    if (firebase.auth.currentUser) {

      const docRef = doc(firebase.db, "users", firebase.auth.currentUser.uid);
      const userData = await getDoc(docRef);

      await addDoc(collection(firebase.db, "requests"),{
        from: firebase.auth.currentUser.uid,
        name: userData.data().displayName,
        to: id
      })
    }
  }

  return (
    <Stack
        spacing={2}
        sx={{
          minHeight: "600px"
        }}
    >
      <Input
      endDecorator={<PersonSearch />} />
      <Stack
        direction="column"
        alignItems="center"
        spacing={2}
        >
        {users?.map((user, index) => {
          if (firebase.auth.currentUser && user.id === firebase.auth.currentUser.uid) {
            return;
          }
          return (
            <Card key={index}
                sx={{
                    width: "200px"
                }}
            >
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography level="h4">{user.data.displayName}</Typography>
                <Tooltip title="Send friend request">
                <IconButton disabled={!firebase.auth.currentUser} onClick={() => {send_request(user.id)}}>
                  <PersonAdd />
                </IconButton>
                </Tooltip>
              </Stack>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography level="body-md">Wins: {user.data.wins}</Typography>
              </Stack>
            </Card>
          );
        })}
      </Stack>
    </Stack>
  );
}
export default PageUsers;
